/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package net.coconauts.template.system;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class AssetsSounds {

    public static Sound shoot;
    public static Music bgMusic;

	public static void load () {
	  	Gdx.app.debug("AssetsSounds","Loading");   
	  	
        Gdx.app.debug("AssetsSounds","loading  sounds");
        shoot = Gdx.audio.newSound(Gdx.files.internal("sounds/shot.wav"));
        bgMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/pears.ogg"));
        bgMusic.setLooping(true);
  
        Gdx.app.debug("AssetsSounds","end loading");
	}

	public static void playSound (Sound sound) {
		if (Options.sound.getBoolean()) sound.play(1);
	}
	
	public static void playMusic(Music music){
//		if (Options.sound.getBoolean()) music.play();
	}
	
	public static void stopMusic(Music music){
//		if (bgMusic.isPlaying()) music.stop();
	}
}
