/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package net.coconauts.template.system;

import java.util.HashMap;
import java.util.Map;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class AssetsSprites {

	public static BitmapFont font;
	public static BitmapFont dynFont;

    public static Sprite player;
    public static Sprite playerDead;
    public static Sprite player_move;
    public static Sprite enemy;
    public static Texture particle;
    public static Sprite wall ;
    public static Sprite warp;
    public static Sprite invalid;
    public static Sprite master;
    
    //icons
    public static Texture soundOn;
    public static Texture soundOff; 
    public static Texture back;
    public static Texture next;
    public static Texture prev;    
    
    //google play services 
    public static Map<String,Texture> leaderboards;
    public static Map<String,Texture> achievements;
    public static Map<String,Texture> play;

    //google play services 
    public static Texture facebook;
    public static Texture twitter;
    public static Texture email;
    public static Texture coconautsLogo;
    public static Texture gplus;
    public static Texture linkedin;

    public static int MAX_SLIDES = 2 ; //number of tutorial slides (physical count, not max index)
    public static Texture[] tutorialSlides;

	public static Texture loadTexture (String file) {
		return new Texture(Gdx.files.internal(file));
	}

	public static void load () {
	  	Gdx.app.debug("AssetsSprites","Loading ");   
	  	
	  	Texture.setEnforcePotImages(false); // Warning: this bypasses the power-of-two requirement for textures.
		// It may break the program for old hardware.
		// See http://stackoverflow.com/questions/14256294/how-to-load-images-as-background-in-libgdx
	  	
	  	//fonts
        Gdx.app.debug("AssetsSprites","loading fonts");
		font = new BitmapFont(Gdx.files.internal("fonts/font.fnt"), Gdx.files.internal("fonts/font.png"), false);
		dynFont = new BitmapFont(Gdx.files.internal("fonts/font.fnt"), Gdx.files.internal("fonts/font.png"), false);

        Gdx.app.debug("AssetsSprites","loading sprites ");
        player = new Sprite(loadTexture("sprites/duck.png"));
//        player.setScale(0.1f);
//        player.scale(1);
        player.setSize(40, 50);
        playerDead = new Sprite(loadTexture("sprites/duck_dead.png"));
        playerDead.setSize(50,40);
        enemy = new Sprite(loadTexture("sprites/baddie.png"));
        enemy.setSize(50,50);
        wall = new Sprite(loadTexture("sprites/wall.png"));
        wall.setSize(50, 50);
        warp = new Sprite(loadTexture("sprites/warp.png"));
        warp.setSize(50,50);
        invalid = new Sprite(loadTexture("sprites/x.png"));
        invalid.setSize(50,50);
        master = new Sprite(loadTexture("sprites/cat.png"));
        master.setSize(50, 60);
        
        particle = loadTexture("sprites/particle.png");
        
        //icons
        Gdx.app.debug("AssetsSprites","loading icons");
        soundOn = loadTexture("icons/soundOn.png");
        soundOff = loadTexture("icons/soundOff.png");
        back = loadTexture("icons/back.png");
        next = loadTexture("icons/next.png");
        prev = loadTexture("icons/prev.png");
        
        //google play services 
        Gdx.app.debug("AssetsSprites","loading  google play services sprites");
        //if (Main.actionResolver.isEnabled()){
	        leaderboards = new HashMap<String,Texture>();
	        achievements= new HashMap<String,Texture>();
	        play= new HashMap<String,Texture>();
	        
	        leaderboards.put("gray", loadTexture("icons/gps/leaderboards_gray.png"));
	        leaderboards.put("green", loadTexture("icons/gps/leaderboards_green.png"));
	        leaderboards.put("white", loadTexture("icons/gps/leaderboards_white.png"));
	        achievements.put("gray", loadTexture("icons/gps/achievements_gray.png"));
	        achievements.put("green", loadTexture("icons/gps/achievements_green.png"));
	        achievements.put("white", loadTexture("icons/gps/achievements_white.png"));
	        play.put("gray", loadTexture("icons/gps/play_gray.png"));
	        play.put("green", loadTexture("icons/gps/play_green.png"));
	        play.put("white", loadTexture("icons/gps/play_white.png"));
        //}
        
        //about
        Gdx.app.debug("AssetsSprites","loading  about sprites");
        facebook = loadTexture("icons/about/facebook.png");
        twitter = loadTexture("icons/about/twitter.png");
        email = loadTexture("icons/about/email.png");
        coconautsLogo = loadTexture("icons/about/coconauts_logo.png");
        gplus = loadTexture("icons/about/gplus.png");
        linkedin  = loadTexture("icons/about/linkedin.png");
            
        
        // tutorial slides
        Gdx.app.debug("AssetsSprites","loading  tutorial slides");
        tutorialSlides = new Texture[MAX_SLIDES];
        
        for (int i=0; i<MAX_SLIDES; i++){
                tutorialSlides[i] = loadTexture("tutorial/tutorial-"+(i+1)+".png");
        }
        
        
        //Settings.load(game.getFileIO());
        Gdx.app.debug("AssetsSprites","end loading");
	}
}
