package net.coconauts.template.system;

import net.coconauts.template.interfaces.Native;
import net.coconauts.template.interfaces.ScoreResolver;
import net.coconauts.template.objects.TextObject;
import net.coconauts.template.screens.MainScreen;

import com.badlogic.gdx.Game;

public class Main extends Game {
	boolean firstTimeCreate = true;
	public static ScoreResolver actionResolver ;
	public static Native nativeFunctions ;

	public Main (ScoreResolver resolver,Native nativeFunctions){
		Main.actionResolver = resolver;
		Main.nativeFunctions = nativeFunctions;
	}
	
	@Override
	public void create () {
		Prefs.reset();
		AssetsSprites.load();
		AssetsSounds.load();
		setScreen(new MainScreen(this));
	}
	
	@Override
	public void render() {
		super.render();
	}
	

	@Override
	public void dispose () {
		super.dispose();
		getScreen().dispose();
	}

	@Override
	public void resize(int width, int height) {}

	@Override
	public void pause() {}

	@Override
	public void resume() {}
}
