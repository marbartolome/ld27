package net.coconauts.template.objects;

import net.coconauts.template.screens.LevelScreen;

import com.badlogic.gdx.math.MathUtils;

public class Enemy extends GameObject{

	public final static int BIG = 2;
	public final static int MID = 1;
	public final static int SMALL = 0;
	public final static float MAX_SPEED = 0.5f;
	public final static float MIN_SPEED = 0.5f;
	public final static int MAX_TYPE = 1;
	
	public float[] projection;
	public float[] iprojection;
	
	public int size;
	public int type;
	
	public Enemy( int size, float x, float y) {
		
		this(size,x,y,0,0);
	}
	
	public Enemy( int size, float x, float y, float dirx, float diry) {

		super(x,y);
		
		this.size = size;
		setVelocity(dirX,dirY);
		
		//Because there are different asteroid types.
		type = MathUtils.random(0, MAX_TYPE);
	}
	
	public void setVelocity(float dirx, float diry){
		if (Math.abs(dirx + diry) < MIN_SPEED) {
			dirX =MathUtils.random(-MAX_SPEED,	MAX_SPEED);
			dirY =MathUtils.random(-MAX_SPEED,	MAX_SPEED);
		} else {

			float factorX = Math.abs(dirx / MAX_SPEED );
			float factorY = Math.abs(diry / MAX_SPEED);
			float maxFactor = Math.max(factorX,factorY);
			
			dirX = dirx / maxFactor;
			dirY = diry / maxFactor;
		}
	}

	public void destroy() {

	}
	@Override
	public void update() {
		super.update();
		move(dirX, dirY);
	}

	// mathematical
	public boolean willCollideDirectly(float x, float y, int radius) {
		/*
		 * Ecuación general de la recta (x - x1)/v1 - (y - y1)/v2 = 0 Ecuación
		 * punto pendiente y -y1 = v2/v1 * (x - x1)
		 */

		float varX = (getCenterX() - x) / dirX;
		float varY = (getCenterY() - y) / dirY;

		return (Math.abs(varX - varY) < radius);
	}

	public boolean willCollideIndirectly(float x, float y, int radius) {
		/*
		 * Ecuación general de la recta (x - x1)/v1 - (y - y1)/v2 = 0 Ecuación
		 * punto pendiente y -y1 = v2/v1 * (x - x1)
		 */

		float varX = (iprojection[0] - x) / dirX;
		float varY = (iprojection[1] - y) / dirY;

		return (Math.abs(varX - varY) < radius);
	}

	public float[] projectionPoint(float x, float y, float dx, float dy) {

		float[] projection = new float[2];

		float cutUp = 0, cutDown = 0, cutLeft = 0, cutRight = 0;

		if (dx > 0) {
			cutRight = calculateCutY(dx, dy, x, y, LevelScreen.WIDTH);
		} else {
			cutLeft = calculateCutY(dx, dy, x, y, 0);
		}

		if (dy > 0) {
			cutDown = calculateCutX(dx, dy, x, y, LevelScreen.HEIGHT);
		} else {
			cutUp = calculateCutX(dx, dy, x, y, 0);
		}
		// a estas alturas, habrá 2 variables inicializadas con el punto de
		// corte, pero solo una de ellas es válida
		if (cutRight > 0 && cutRight < LevelScreen.HEIGHT) {
			projection[0] = LevelScreen.WIDTH;
			projection[1] = cutRight;// (cutRight,0);
		} else if (cutLeft > 0 && cutLeft < LevelScreen.HEIGHT) {
			projection[0] = 0;
			projection[1] = cutLeft;// (cutRight,0);
		} else if (cutUp > 0 && cutUp < LevelScreen.WIDTH) {
			projection[0] = cutUp;
			projection[1] = 0;// (cutRight,0);
		} else if (cutDown > 0 && cutDown < LevelScreen.WIDTH) {
			projection[0] = cutDown;
			projection[1] = LevelScreen.HEIGHT; // (cutRight,0);
		} else {
			projection[0] = 0;
			projection[1] = 0;
		}

		return projection;
	}

	public float[] inverseProjection(float[] p) {

		float[] ip = p.clone();
		if (ip[0] == 0) {
			ip[0] = LevelScreen.WIDTH;
		} else if (ip[0] == LevelScreen.WIDTH) {
			ip[0] = 0;
		}
		if (ip[1] == 0) {
			ip[1] = LevelScreen.HEIGHT;
		} else if (ip[0] == LevelScreen.HEIGHT) {
			ip[1] = 0;
		}
		return ip;
	}

	public float calculateCutX(float dx, float dy, float x0, float y0, float ky) {

		// Ecuación punto pendiente y -y1 = v2/v1 * (x - x1) ->
		// y = dy/dx ( x - x0) + y0
		// Ecuación del plano
		// y = ky
		// Igualamos
		// dy/dx ( x - x0) + y0 = ky
		// v*x - v*x0 - y0 = ky
		// x = (ky + v*x0 - y0) / v

		float v = dy / dx;
		return (ky + v * x0 - y0) / v;
	}

	public float calculateCutY(float dx, float dy, float x0, float y0, float kx) {
		/*
		 * Ecuación punto pendiente y -y1 = v2/v1 * (x - x1) -> y = y0 = v*x -
		 * v*x0 -> x= y - y0 + v*x0 / v Ecuación del plano y x = kx Igualamos (y
		 * * - y0 + v*x0 )/ v = kx y = kx*v +y0 - v*x0
		 */
		float v = dy / dx;
		return kx * v + y0 - v * x0;
	}

	public boolean willCollide(float x, float y, int radius) {

		boolean directly = willCollideDirectly(x, y, radius);
		//boolean indirectly = willCollideIndirectly(x, y, radius);

		return directly;// || indirectly;
	}

}
