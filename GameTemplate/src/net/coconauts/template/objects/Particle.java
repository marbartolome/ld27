
package net.coconauts.template.objects;

import com.badlogic.gdx.math.MathUtils;

public class Particle extends GameObject {
	
	public static final int STATE_ALIVE = 0;	// particle is alive
	public static final int STATE_DEAD = 1;		// particle is dead
	
	public static final long DEFAULT_LIFETIME 	= 500;	// Time life in MS
	public static final int MAX_DIMENSION		= 5;	// the maximum width or height
	public static final int MAX_SPEED			= 10;	// maximum speed (per update)
	
	private int state;			// particle is alive or dead
	public float widht;		// width of the particle
	public float height;		// height of the particle
	private long init_time;		// initial time

	public boolean isAlive() {
		return this.state == STATE_ALIVE;
	}
	public boolean isDead() {
		return this.state == STATE_DEAD;
	}

	public Particle(int x, int y) {
		
		super(x,y);
		
		this.state = Particle.STATE_ALIVE;
		this.widht = MathUtils.random(1, MAX_DIMENSION);
		this.height = this.widht;
		this.init_time = System.currentTimeMillis();

		dirX = MathUtils.random(-MAX_SPEED,	MAX_SPEED);
		dirY =MathUtils.random(-MAX_SPEED,	MAX_SPEED);
		this.alpha = 1;
	}


	public void update() {
		if (this.state != STATE_DEAD) {
			move(dirX, dirY);
			long actual_time = System.currentTimeMillis();
			// extract alpha

			float living_time =(float) ( actual_time - init_time);
			float remaining_time = living_time / DEFAULT_LIFETIME;
			alpha = DEFAULT_LIFETIME - remaining_time;
			
			if (actual_time - init_time >= DEFAULT_LIFETIME) {	// reached the end if its life
				this.state = STATE_DEAD;
			}
		}
	}

	@Override
	public String toString() {
		return "Particle [state=" + state + ", widht=" + widht + ", height="
				+ height + ", x=" + x + ", y=" + y + "]";
	}

}
