package net.coconauts.template.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.math.Rectangle;
public class TextObject  extends GameObject {
	
	public static final int ALIGN_LEFT = 0;
	public static final int ALIGN_CENTER = 1 ;
	public static final int ALIGN_RIGHT = 2;
	
	public String str;
	public int align;
	public float scale ;
	Color color;
	BitmapFont font ;
	
	public TextObject(String str, BitmapFont font , float x, float y) {
		super( x, y);
		
		this.str = str;
		this.font = font;
		this.align =ALIGN_LEFT;
		this.scale = 1;
		this.color = Color.BLACK;

		setRectangle();
	}
	
	public TextObject(String str,BitmapFont font ,float x, float y, int align, float scale, Color color) {
		this(str,font, x, y);
		
		this.align =align;
		this.scale =scale;
		this.color =color;
		
		setRectangle();
	}
	public void setRectangle(){
		font.setScale(this.scale);
		TextBounds bounds = font.getBounds(str);
		super.setRectangle( bounds.width,bounds.height);
		
		this.x =  alignTextX();
	}
	
	@Override
	public void draw(SpriteBatch batch ){
		font.setColor(color);
		font.setScale(scale);
		
		font.drawMultiLine(batch, str, x, y);
	}
	
	@Override
	public Rectangle getRectangle(){
		return new Rectangle(x,y-h,w,h);
	}

	public int alignTextX(){
		 float alignment = 0;
		 switch (align){
			 case ALIGN_CENTER: alignment =w / 2 ; break;
			 case ALIGN_RIGHT: alignment = w; break;
		 }
		 return  (int) (x - alignment) ;
	}
}