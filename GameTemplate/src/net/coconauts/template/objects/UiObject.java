package net.coconauts.template.objects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;


public class UiObject extends GameObject {
	
	public UiObject(Texture t, float x, float y, float w, float h) {
		super(t, x, y, w, h);
	}
	
	public UiObject(Sprite s,  float x,float y){
		super(s,x,y);
	}
}
