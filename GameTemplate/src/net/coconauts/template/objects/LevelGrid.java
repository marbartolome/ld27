package net.coconauts.template.objects;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import net.coconauts.template.screens.LevelScreen;
import net.coconauts.template.system.AssetsSprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class LevelGrid {

	public class GridCell{
		public Set<GameObject> contents = new HashSet<GameObject>();
		
		public float screenXPos;
		public float screenYPos;
		
		public int gridXindex;
		public int gridYindex;
		
		public float cellXsize;
		public float cellYsize;
		
		public LevelGrid containerGrid;
		
		public GridCell(int gridXindex, int gridYindex, LevelGrid containerGrid){
			this.containerGrid = containerGrid;
			
			this.cellXsize = containerGrid.cellXSize;
			this.cellYsize = containerGrid.cellYSize;
			this.gridXindex = gridXindex;
			this.gridYindex = gridYindex;
			
			this.screenXPos = gridXindex*cellXSize;
			this.screenYPos = gridYindex*cellYSize;

		}
		
		public void addElement(GameObject o){
			contents.add(o);
			o.x = screenXPos;
			o.y = screenYPos;
		}
		
		public boolean isWarp(){
			for (GameObject o : contents){
				if( o instanceof Warp) return true;
			}
			return false;
		}
		
		public Warp getWarp(){
			for (GameObject o : contents){
				if( o instanceof Warp) return (Warp)o;
			}
			return null;
		}
		
		public boolean isWall(){
			for (GameObject o : contents){
				if( o instanceof Wall) return true;
			}
			return false;
		}
		
		public boolean isEmpty(){
			return contents.isEmpty();
		}
		
	}

	
	public GridCell[][] grid;
	public int xDim;
	public int yDim;
	public float cellXSize;
	public float cellYSize;
	
	
	// Convenient locators for our game objects
	public GridCell playerCell;
	public Set<GridCell> baddies;
	public Set<GridCell> walls;
	public List<GridCell> warps;
	
	Random randGen = new Random();
	
	public LevelScreen containerLevel;
	
	public boolean isMaster;
	
	public static LevelGrid masterLevelFactory(int dimX, int dimY, float cellXSize, float cellYSize){
		LevelGrid masterGrid = new LevelGrid(dimX, dimY, cellXSize, cellYSize);
		
		// place walls in the border
		for (int iy = 0; iy<dimY; iy++){
			if (iy==0 || iy==dimY-1){
				// first and last rows, fill them up completely
				for(int ix=0; ix<dimX; ix++){
					masterGrid.grid[iy][ix].addElement(new Wall());
					masterGrid.walls.add(masterGrid.grid[iy][ix]);
				}
			} else {
				// middle rows, just fill first and last column
				masterGrid.grid[iy][0].addElement(new Wall());
				masterGrid.walls.add(masterGrid.grid[iy][0]);
				masterGrid.grid[iy][dimX-1].addElement(new Wall());
				masterGrid.walls.add(masterGrid.grid[iy][dimX-1]);
			}
		}
		
		
		int centerXindex = dimX/2;
		int centerYindex = dimY/2;
		int downishYindex = dimY/3;
		
		// place a warp in the center-down-ish
		masterGrid.grid[downishYindex][centerXindex].addElement(new Warp());
		masterGrid.warps.add(masterGrid.grid[downishYindex][centerXindex]);
		
		
		// place the master of the universe in the center
		GridCell masterCell = masterGrid.grid[centerYindex][centerXindex];
		masterCell.addElement(new UiObject(AssetsSprites.master, 0f,0f));
		
		masterGrid.isMaster = true;
		
		return masterGrid;
		
	}
	
	
	public LevelGrid(int dimX, int dimY, float cellXSize, float cellYSize){
		this.xDim = dimX;
		this.yDim = dimY;
		grid = new GridCell[dimX][dimY];
		this.cellXSize = cellXSize;
		this.cellYSize = cellYSize;
		
		for (int iy = 0; iy<dimY; iy++){
			for(int ix=0; ix<dimX; ix++){
				GridCell cell = new GridCell(ix, iy, this);
				
				grid[iy][ix] = cell;
			}
		}
		
		baddies = new HashSet<GridCell>();
		walls = new HashSet<GridCell>();
		warps = new ArrayList<GridCell>();
		
		isMaster = false;
	}
	
	/**
	 * Number of cells in the grid
	 * @return
	 */
	public int getGridSize(){
		return grid.length*grid[0].length;
	}
	
	
//	public GameObject randomElement(){
//		int i = randGen.nextInt(3);
//		switch(i){
//			case(0): return new Wall();
//			case(1): return new Warp();
//			case(2): return new Baddie();
//		}
//		
//		return null; // this should never be reached though
//	}
	
	
	public void populate(){
		
		// Hard coded restrictions for now
//		int maxWalls = (int) ( ((float)getGridSize())*0.4);
//		int maxBaddies = 
		
		int percentWalls = 3;
		int percentBaddies = 1;
				
		
		for (GridCell[] row : grid){
			for(GridCell cell : row){
				int i = randGen.nextInt(100);
				if (i<30){
					cell.addElement(new Wall());
					walls.add(cell);
//				} else if (i<35) {
//					cell.addElement(new Baddie());
//					baddies.add(cell);
//				} else if (i<37) {
				} else if (i<33){
					cell.addElement(new Warp());
					warps.add(cell);
				} else {
					// empty cell
				}
			}
		}
		
		// Do we have warps at all? if not, force one
		if (warps.size()==0){
			GridCell cell = ramdonEmptyCell();
			cell.addElement(new Warp());
			warps.add(cell);
		}
		
	}
	
	public void draw(SpriteBatch batch){
		for (GridCell[] row : grid){
			for(GridCell cell : row){
				for(GameObject go : cell.contents){
					go.draw(batch);
				}
			}
		}
	}
	
	public void placePlayer(Player p, int x, int y){
		try{
			placePlayer(p,grid[y][x]);
		} catch(ArrayIndexOutOfBoundsException e){
			Gdx.app.log("move", "Can't move outside the grid");
		}
		
		
	}
	
	public void placePlayer(Player p, GridCell cell){
		if (cell == null){
			// remove player from this grid
			//remove player from current cell
			if(playerCell!=null) {
				playerCell.contents.remove(p);
				// If leaving a warp cell, reactivate
				if(playerCell.isWarp()) playerCell.getWarp().activate();
			}
			playerCell = null;
			return;
		}
		if (cell.contents.isEmpty()){
			//empty cell
			//remove player from current cell
			if(playerCell!=null) {
				playerCell.contents.remove(p);
				// If leaving a warp cell, reactivate
				if(playerCell.isWarp()) playerCell.getWarp().activate();
			}
			// put player in new cell
			cell.addElement(p);
			playerCell = cell;
			
		} else if (cell.contents.toArray()[0] instanceof Warp){
			// warp cell
			//remove player from current cell
			if(playerCell!=null){
				playerCell.contents.remove(p);
				// If leaving a warp cell, reactivate
				if(playerCell.isWarp()) playerCell.getWarp().activate();
			}
			// put player in new cell
			cell.addElement(p);
			playerCell = cell;
		}
		
		// otherwise it can't be moved here
		else {
			Gdx.app.log("move", "can't place payer on "+cell.gridXindex+","+cell.gridYindex);
		}
	}
	
	public GridCell getCellFromScreenCoords(int screenXpos, int screenYpos){
		int cellx = ((int) (screenXpos/cellXSize)) % this.xDim;
		int celly = ((int) (screenYpos/cellYSize)) % this.yDim;
		
		return grid[celly][cellx];
	}
	
	public GridCell getCellFromGridCoords(int gridXindex, int gridYindex){
		return grid[gridYindex][gridXindex];
	}
	
	public boolean isEmpty(GridCell cell){
		return cell.contents.isEmpty();
	}
	
	public boolean isInlineWithPlayer(GridCell cell){
		return playerCell.gridXindex==cell.gridXindex || playerCell.gridYindex==cell.gridYindex;
	}
	
	
	public GridCell ramdonEmptyCell(){
		int ix = randGen.nextInt(xDim);
		int iy = randGen.nextInt(yDim);
		GridCell pick = grid[iy][ix];
		
		while (!pick.contents.isEmpty()){
			ix = randGen.nextInt(xDim);
			iy = randGen.nextInt(yDim);
			pick = grid[iy][ix];
		}
		
		return pick;
				
	}
	
	public int xDirFromPlayer(GridCell cell){
		if (cell.gridXindex > playerCell.gridXindex) return 1;
		else if (cell.gridXindex < playerCell.gridXindex) return -1;
		else return 0;
	}
	
	public int yDirFromPlayer(GridCell cell){
		if (cell.gridYindex > playerCell.gridYindex) return 1;
		else if (cell.gridYindex < playerCell.gridYindex) return -1;
		else return 0;
	}
	
}
