package net.coconauts.template.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;

import net.coconauts.template.screens.LevelScreen;

public abstract class GameObject {
	
	public Sprite sprite;
	public float x;
	public float y;
	public float origX;
	public float origY;
	public boolean visible = true;
	public boolean rendered = true;
	public float w;
	public float h;
	public float rotation;
	public float dirX;
	public float dirY;
	public float alpha; //from 0 to 1

	public long initTime;
	
	public GameObject( float x,float y){
		
		this.x = x; this.origX = x;
		this.y = y; this.origY = y;
		w = 0;	h = 0;
		rotation = 0;
		alpha = 1;
		initTime= System.currentTimeMillis();
	}
	
	public GameObject(Texture t,  float x,float y){
		this(new Sprite(t), x, y);
	}
	public GameObject(Texture t,  float x,float y, float w, float h){
		this(new Sprite(t), x, y, w, h);
	}
	
	public GameObject(Sprite s,  float x,float y){
		this(x,y);
		setSprite(s);
	}
	public GameObject(Sprite s,  float x,float y, float w, float h){
		this(x,y);
		setSprite(s);
		sprite.setSize(w,h);
		setRectangle(w, h);
	}
	
	public void setSprite(Sprite t){
		sprite = t;
		w = t.getWidth();
		h = t.getHeight();
	}
	public void setRectangle(float w, float h){
		this.w = w;
		this.h = h;
	}
	
	public Rectangle getRectangle(){
		return new Rectangle(x,y,w,h);
	}
	
	public void move(float dx, float dy){
		x = dx;
		y = dy;
		
		Gdx.app.log("move", dx+","+dy);

			
		
//		if (x > LevelScreen.WIDTH)
//			x = 30;
//		else if (x < 0)
//			x = LevelScreen.WIDTH - 30;
//		else
//			x += dx;
//		if (y > LevelScreen.HEIGHT)
//			y = 30;
//		else if (y < 0)
//			y = LevelScreen.HEIGHT - 30;
//		else
//			y += dy;
	}
	
	public float distance(float x, float y) {
		//Distance formula d = sqrt( (x- x1)^2 + (y - y1)^2 )
		
		float varX = getCenterX() - x;
		float varY = getCenterY() - y;
		varX = varX * varX;
		varY = varY * varY;
		return (float) Math.sqrt(varX + varY);

	}

	public float getBottom(){
		return this.y+h;
	}
	public float getRight(){
		return this.x+w;
	}
	public float getCenterX(){
		return this.x+w/2;
	}
	public float getCenterY(){
		return this.y+h/2;
	}

	public boolean overlaps(GameObject s){
		return getRectangle().overlaps(s.getRectangle());
	}
	
	public boolean collide(GameObject s){
		boolean overlaps = this.overlaps(s);
		boolean isOverlaped = s.overlaps(this);
		return overlaps || isOverlaped;
	}
	
	public void draw(SpriteBatch batch ){
		
		if (rendered && visible){
			sprite.setPosition(x,y);
			sprite.draw(batch);
		}
	}
	
	public boolean click(float x, float y ){
		return (getRectangle().contains(x, y));
	}
	public void update(){
		if (!rendered) return;
	}
	
	public void drawBorder(ShapeRenderer renderer) {
		renderer.begin(ShapeType.Rectangle);
		Rectangle r = getRectangle();
		renderer.rect(r.x,r.y,r.width,r.height);
		renderer.end();
	}
	public long lifeTimeMillis(){
		return System.currentTimeMillis() - initTime;
	}
}
