package net.coconauts.template.objects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import net.coconauts.template.system.AssetsSprites;
import net.coconauts.template.system.Options;

public class Warp extends GameObject {

	public boolean active;
	Sprite inactive;
	
	public Warp() {
		super(AssetsSprites.warp, 0f,0f);
		active = true;
		inactive = AssetsSprites.invalid;
		// TODO Auto-generated constructor stub
	}
	
	public void toggle(){
		active = !active;
	}
	
	public void activate(){
		active = true;
	}
	
	public void deactivate(){
		active = false;
	}

	@Override
	public void draw(SpriteBatch batch) {
		// TODO Auto-generated method stub
		
		if(Options.debug.getBoolean() && !active){
			inactive.setPosition(x,y);
			inactive.draw(batch);
		} else {
			super.draw(batch);
		}
		
		
//		Color c = new Color(batch.getColor());
//		batch.setColor(Color.BLUE); 
//		super.draw(batch);
//		batch.setColor(c);
	}

}
