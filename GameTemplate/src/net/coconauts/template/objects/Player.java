package net.coconauts.template.objects;

import net.coconauts.template.system.AssetsSprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class Player extends GameObject {
	
	public boolean dead = false;

	public Player(float posX, float posY) {
		super(posX, posY);
	}
	public Player(Sprite t,  float x,float y){
		super(t, x, y);
	}
	
	public Player(){
		super(AssetsSprites.player,0f,0f);
	}

	@Override
	public void update(){
		super.update();
	}
	
	@Override
	public void draw(SpriteBatch batch) {
		// TODO Auto-generated method stub
		if (!dead){
			super.draw(batch);
		} else {
			AssetsSprites.playerDead.setPosition(x,y);
			AssetsSprites.playerDead.draw(batch);
		}

	}
}
