/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package net.coconauts.template.screens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import net.coconauts.template.controllers.GameInput;
import net.coconauts.template.interfaces.ScoreResolver.Achievements;
import net.coconauts.template.objects.LevelGrid;
import net.coconauts.template.objects.LevelGrid.GridCell;
import net.coconauts.template.objects.Player;
import net.coconauts.template.objects.TextObject;
import net.coconauts.template.objects.Warp;
import net.coconauts.template.system.AssetsSounds;
import net.coconauts.template.system.AssetsSprites;
import net.coconauts.template.system.Main;
import net.coconauts.template.system.Options;
import net.coconauts.template.system.Prefs;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

public class GameScreen implements Screen {
	
	public Game game;
	public LevelScreen level;
	public OrthographicCamera cam;
	public SpriteBatch batch;
//	public Vector3 touchPoint;
	public ShapeRenderer renderer ;

	public static int WIDTH;
	public static int HEIGHT;
	
	public static int score = 0;
	public long startTime = System.currentTimeMillis(); 
	public boolean gameEnded = false;
	public boolean gameOver = false;
	public int nLevel = 0;
	
	public GameInput input; 
	
	public Rectangle rBack = new Rectangle(10,10,64,64);
	
	public TextObject debugTxt;
	public TextObject masterDialogue;
	public TextObject gameOverText;
	
	public List<LevelGrid> levelGrids;
	public List<LevelScreen> levelScreens;
	public Map<LevelGrid.GridCell, LevelGrid.GridCell> warpGraph;
	
	public int maxLevels = 3;
	
	private long timeProbe = System.currentTimeMillis();
	
	public GameScreen (Game game, int gameMode) {
		this.game = game;
		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);  
        cam.position.set(WIDTH / 2, HEIGHT / 2, 0);

		batch = new SpriteBatch();
		renderer = new ShapeRenderer();
		
		
		
		
		debugTxt = new TextObject( "", AssetsSprites.font,10,HEIGHT-10, TextObject.ALIGN_LEFT,0.5f,Color.WHITE);
		masterDialogue = new TextObject( "", AssetsSprites.font,60,HEIGHT-100, TextObject.ALIGN_CENTER,1f,Color.WHITE);
		gameOverText = new TextObject("Game Over", AssetsSprites.font, 30, HEIGHT-250, TextObject.ALIGN_LEFT, 3.5f, Color.WHITE);
				
		// Generate the level graph
		levelGrids = new ArrayList<LevelGrid>();
		levelScreens = new ArrayList<LevelScreen>();
		warpGraph = new HashMap<LevelGrid.GridCell,LevelGrid.GridCell>();
		
		initLevelGraph();
		
		// Init the player
		Player player = new Player(AssetsSprites.player, 0f,0f);
//		player.setRectangle(AssetsSprites.player);
		
		// make a level screen for each level grid
		for (LevelGrid grid : levelGrids) levelScreens.add(new LevelScreen(grid,player));
		
		// Pick a level to be the initial one,
		// and place the player on it.
		level = levelScreens.get(0);
		level.grid.placePlayer(player, level.grid.ramdonEmptyCell());
		
		input = new GameInput(this);
		
		Prefs.reset();
		
		AssetsSounds.bgMusic.play();
		
	}
	
//	public void logLevelGraph(){
//		for (LevelGrid.GridCell warpOrigin : warpGraph.keySet()){
//			Gdx.app.log("warpgraph", warpOrigin.toString() )
//		}
//	}
	
	public void initLevelGraph(){
		// Make levels, with dummy warps
		for (int i=0; i<maxLevels; i++){
			LevelGrid lGrid = new LevelGrid(12,12, 50, 50);
			lGrid.populate();
			levelGrids.add(lGrid);
			for (LevelGrid.GridCell warpCell : lGrid.warps){
				warpGraph.put(warpCell, null);
			}
		}
		
		// Make level screens with all 


		// Link warps
		LevelGrid.GridCell prevOrigin = null;
		for (LevelGrid.GridCell warpOrigin : warpGraph.keySet()){
			// Iterate origins by pairs, and link them together.
			// Since we're iterating a set, results will come out of order, exactly what we need :o)
			if (prevOrigin!=null){
				// link prev with current and vice versa
				warpGraph.put(prevOrigin, warpOrigin);
				warpGraph.put(warpOrigin, prevOrigin);
				prevOrigin = null;
			} else {
				// store current origin as prev to use in next iteration
				prevOrigin = warpOrigin;
			}
		}
				
		
		// After this step, if we had an even amount of warps they will all be linked.
		// Otherwise, one of the warps will link to "null".
		// If this is the case, link that one to the master warp door.
		// Otherwise make a new warp in a level somewere and point it to the master
		LevelGrid masterGrid = LevelGrid.masterLevelFactory(12, 12, 50, 50);
		LevelGrid.GridCell masterWarp = (LevelGrid.GridCell) masterGrid.warps.toArray()[0];
		if (prevOrigin != null){
			warpGraph.put(prevOrigin, masterWarp);
			
			
//			warpGraph.remove(prevOrigin);
//			prevOrigin.contents.clear();
		} else {
			// make up an extra warp and link to to the master
			LevelGrid.GridCell extraWarpCell = levelGrids.get(levelGrids.size()-1).ramdonEmptyCell();
			extraWarpCell.addElement(new Warp());
			extraWarpCell.containerGrid.warps.add(extraWarpCell);
			
			warpGraph.put(extraWarpCell, masterWarp);
		}
		
		// add master level to list of grids
		levelGrids.add(masterGrid);

	}

	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////
	
	@Override
	public void render (float delta) {
		level.render(delta);
		
		update(delta);
		
		draw(delta);
		
		
	}

	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
		
	public void draw (float deltaTime) {
		GLCommon gl = Gdx.gl;
//		gl.glClearColor(0, 0, 0, 1);
//		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		cam.update();
		batch.setProjectionMatrix(cam.combined);

		batch.begin();
		//batch.setColor(1,1,1,1);
		//AssetsSprites.font.setColor(1,1,1,1);
	 
		if (Options.gui.getBoolean()) drawGUI();
		if (Options.debug.getBoolean() == true) drawDebug();
		if (gameEnded || gameOver ) drawEnd();
		
		batch.end();
		
		//SHAPE RENDERING
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		renderer.setProjectionMatrix(cam.combined);		
	}

	public void drawGUI(){
		batch.draw(AssetsSprites.back, 10,10,64,64);
		
		//draw score(score);
	}
	
	public void drawDebug(){
		debugTxt.draw(batch);
	}
	
	public void drawEnd(){
		//Utils.drawText(batch, AssetsSprites.font, "Game Over ", WIDTH/2, HEIGHT/2 , Utils.ALIGN_CENTER,2,new Color(1, 0, 0, 1));
		if(gameEnded) masterDialogue.draw(batch);
		if(gameOver) gameOverText.draw(batch);
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void update (float deltaTime) {
		// Actual game logic
		updateLevelCounter();
		nextLevel();
		gameEnd();
		debug();
	}
	
	public void updateLevelCounter(){
		// Increase an achievement every minute. 
        if (System.currentTimeMillis()-timeProbe>1000){
        	level.tick();
        	// and reset the probe
            timeProbe = System.currentTimeMillis();
        }
	}
	
	public void debug(){
		debugTxt.str = "player @ "+level.player.x+","+level.player.y+"   "+
				+ level.grid.getCellFromScreenCoords((int)level.player.x, (int)level.player.y).gridXindex+","+level.grid.getCellFromScreenCoords((int)level.player.x,(int)level.player.y).gridYindex+"\n"
				+getMouseX()+","+getMouseY()+"   "+
				+level.grid.getCellFromScreenCoords(getMouseX(), getMouseY()).gridXindex+","+level.grid.getCellFromScreenCoords(getMouseX(), getMouseY()).gridYindex+"\n"
				+level.grid.walls.size()+" walls  "+level.grid.baddies.size()+" enems  "+level.grid.warps.size()+" warps";
	}
	
	public int getMouseX(){
		return Gdx.input.getX();
	}
	
	public int getMouseY(){
		return HEIGHT - Gdx.input.getY();
	}
	
	public void gameEnd(){
		// Check if the current grid is the master grid
		// placed last one in our list
		if(level.grid.equals(levelGrids.get(levelGrids.size()-1))){
			gameEnded = true;
			for (GridCell cell : level.grid.warps){
				cell.contents.remove(cell.getWarp());
			}
			level.grid.warps.clear();
//			level.grid.warps.toArray(new LevelGrid.GridCell[0])[0].contents.clear();
//			level.grid.warps.get(0).contents.clear();
//			level.grid.warps.clear();
			masterDialogue.str = "I am the MASTER OF ALL MAGIC!\n" +
					"          Fancy a cup of tea?";
		}
		
		// Check if you ran out of time: then it's game over!
		if(level.countdown == 0){
			gameOver = true;
			level.player.dead = true;
		}
		
		
//		int gameScore = 0;
//		
//		gameEnded = true;
//		Main.actionResolver.unlockAchievement(Main.actionResolver.getAchievementId(Achievements.FINISH_ARCADE));
//		
//		// Compute and submit score
//		//gameScore = nLevel*1000+normalizeAsteroidScore(score, getInitialAsteroidPoints(nLevel));
//		Main.actionResolver.submitScore(Main.actionResolver.getScoreboardArcade(), gameScore);				
			
	}
	public void nextLevel(){
		warp();
		
		
		
		
//		if (level.isLevelEnded() ){
//			nLevel++;
//
//		}
	}
	
	public void warp(){
		// check if the player is on a warp cell.
		// If so, teleport it to the next grid
		if ( level.grid.playerCell.isWarp() && level.grid.playerCell.getWarp().active){
			// engage warp!
			LevelGrid.GridCell destinyWarp = warpGraph.get(level.grid.playerCell); //get destiny
			level.grid.placePlayer(level.player, null); //remove from current grid (place nowhere)
			
			//switch to the new level
			level = destinyWarp.containerGrid.containerLevel;
			
//			level.grid = destinyWarp.containerGrid; // set new grid
			destinyWarp.getWarp().deactivate(); //deactivate destiny warp to prevent warpback
			level.grid.placePlayer(level.player, destinyWarp); // place player in new grid
			
		}
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.debug(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.debug(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.debug(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.debug(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.debug(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.debug(this.getClass().getName(), "hide");	}
}
