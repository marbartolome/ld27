package net.coconauts.template.screens;

import net.coconauts.template.controllers.MainInput;
import net.coconauts.template.objects.GameObject;
import net.coconauts.template.objects.TextObject;
import net.coconauts.template.objects.UiObject;
import net.coconauts.template.system.AssetsSprites;
import net.coconauts.template.system.Main;
import net.coconauts.template.system.Options;
import net.coconauts.template.system.Prefs;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector3;

public class MainScreen implements Screen {

	public Game game;
	public LevelScreen level;
	public static OrthographicCamera cam;
	public SpriteBatch batch;
	public Vector3 touchPoint;
	public ShapeRenderer renderer;
	public MainInput input ;
	
	TextObject debugTxt;

	
	public static int WIDTH;
	public static int HEIGHT;

	//Ui Objects
//	public GameObject uiGPSLogin;
//	public GameObject uiGPSLogout ;
//	public GameObject uiAchievements ;
//	public GameObject uiLeaderboards;
	//Text objects
	public GameObject textGameTitle ;
	public GameObject textPlay ;
//	public GameObject textTutorial;
//	public GameObject textAbout ;
	
	public MainScreen(Game game) {
		this.game = game;

		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.position.set(WIDTH / 2, HEIGHT / 2, 0);

		batch = new SpriteBatch();
		renderer = new ShapeRenderer();
		this.level = new LevelScreen();
		this.input = new MainInput(this);

		initObjects();
	}
	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	public void initObjects(){
		
		
//		uiGPSLogin = new UiObject(AssetsSprites.play.get("white"),200, 10,64,64);
//		uiGPSLogout = new UiObject(AssetsSprites.play.get("green"),200, 10,64,64);
//		uiAchievements = new UiObject(AssetsSprites.achievements.get("green"), 300, 10,64,64);
//		uiLeaderboards = new UiObject(AssetsSprites.leaderboards.get("green"), 400, 10,64,64);
		//Text objects
		textGameTitle = new TextObject(" The Master\nof All Magic", AssetsSprites.font, 30, HEIGHT - 50, TextObject.ALIGN_LEFT,3,Color.WHITE);
		textPlay = new TextObject("Play", AssetsSprites.font, WIDTH/2,  HEIGHT/2, TextObject.ALIGN_CENTER,2,Color.WHITE);
//		textTutorial = new TextObject( "Tutorial", AssetsSprites.font, WIDTH/2,  250,TextObject.ALIGN_CENTER,2,Color.WHITE);
//		textAbout = new TextObject( "About", AssetsSprites.font, WIDTH/2,  150, TextObject.ALIGN_CENTER,2,Color.WHITE);
		
		debugTxt = new TextObject( "", AssetsSprites.font,10,HEIGHT-10, TextObject.ALIGN_LEFT,0.5f,Color.WHITE);
		
	}
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void render(float delta) {

		update(delta);
		draw(delta);

//		level.render(delta);
	}
	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
	public void draw(float deltaTime) {
		GLCommon gl = Gdx.gl;
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		cam.update();
		batch.setProjectionMatrix(cam.combined);

		//batch.disableBlending();
		batch.begin();
		
			drawMenu();
			if (Options.debug.getBoolean() == true) drawDebug();
			

		batch.end();

		// SHAPE RENDERING
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glLineWidth(5);
		renderer.setProjectionMatrix(cam.combined);
		
//		 renderer.begin(ShapeType.Line);
////		 renderer.setColor(1, 1, 0, 1);
//		 renderer.setColor(Color.WHITE);
//		 renderer.line(10,10, WIDTH-10, HEIGHT-10);
//		 renderer.line(10, HEIGHT-10, WIDTH-10, 10);
//		 renderer.end();

		
	}

	public void drawMenu() {

		textGameTitle.draw(batch);
		textPlay.draw(batch);
//		textAbout.draw(batch);
//		textTutorial.draw(batch);
		
		//if (Main.actionResolver.isEnabled()){
//			if (Main.actionResolver.isSigned()){
//				uiGPSLogout.draw(batch);
//				uiAchievements.draw(batch);
//				uiLeaderboards.draw(batch);
//			} else {
//				uiGPSLogin.draw(batch);
//			}
		//}
	}
	
	public void drawDebug(){
		debugTxt.draw(batch);
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void update(float deltaTime) {
		debugTxt.str = ""+HEIGHT+","+WIDTH;
	}
	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.debug(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.debug(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.debug(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.debug(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.debug(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.debug(this.getClass().getName(), "hide");	}
}
