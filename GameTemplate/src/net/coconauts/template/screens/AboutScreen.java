package net.coconauts.template.screens;

import net.coconauts.template.controllers.AboutInput;
import net.coconauts.template.interfaces.ScoreResolver.Achievements;
import net.coconauts.template.objects.GameObject;
import net.coconauts.template.objects.TextObject;
import net.coconauts.template.objects.UiObject;
import net.coconauts.template.system.AssetsSprites;
import net.coconauts.template.system.Main;
import net.coconauts.template.system.Options;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;

public class AboutScreen implements Screen {

	public Game game;
	public LevelScreen level;
	public static OrthographicCamera cam;
	public SpriteBatch batch;
	public Vector3 touchPoint;
	public ShapeRenderer renderer;

	public static int WIDTH;
	public static int HEIGHT;

	public AboutInput input; 
	
	//Ui Objects
	public GameObject uiCoconautsLogo ; // new UiObject(AssetsSprites.coconautsLogo, WIDTH/2-128,HEIGHT-300,256,256);
	public GameObject uiBack ; // new UiObject(AssetsSprites.back, 10,10,64,64);

	public GameObject uiLinkedinRephus ; // new UiObject(AssetsSprites.linkedin, 260, HEIGHT-520,32,32);
	public GameObject uiEmailRephus ; // new UiObject(AssetsSprites.email, 300, HEIGHT-520,32,32);
	public GameObject uiFacebookRephus ; // new UiObject(AssetsSprites.facebook, 340, HEIGHT-520,32,32);
	public GameObject uiTwitterRephus ; // new UiObject(AssetsSprites.twitter, 380, HEIGHT-520,32,32);
	
	public GameObject uiGPlusRephus ; // new UiObject(AssetsSprites.gplus, 420, HEIGHT-520,32,32);

	public GameObject uiLinkedinMar ; // new UiObject(AssetsSprites.linkedin, 260, HEIGHT-570,32,32);
	public GameObject uiEmailMar ; // new UiObject(AssetsSprites.email, 300, HEIGHT-570,32,32);
	public GameObject uiFacebookMar ; // new UiObject(AssetsSprites.facebook, 340, HEIGHT-570,32,32);
	public GameObject uiTwitterMar ; // new UiObject(AssetsSprites.twitter, 380, HEIGHT-570,32,32);
	public GameObject uiGPlusMar ; // new UiObject(AssetsSprites.gplus, 420, HEIGHT-570,32,32);
	
	//Text objects
	public GameObject textCoconauts ; // new TextObject("Coconauts",  AssetsSprites.font, WIDTH/2, HEIGHT-330, TextObject.ALIGN_CENTER,2,Color.WHITE);
	public GameObject textCoconautsUrl ; // new TextObject("http://coconauts.net", AssetsSprites.font, WIDTH/2, HEIGHT-380,  TextObject.ALIGN_CENTER,1,Color.BLUE);
	public GameObject textGameDeveloped ; // new TextObject("A game developed by", AssetsSprites.font, WIDTH/2, HEIGHT-450,  TextObject.ALIGN_CENTER,1,Color.WHITE);
	public GameObject textJavier ; // new TextObject("Javier Rengel", AssetsSprites.font,10, HEIGHT-500);
	public GameObject textMar ; // new TextObject("Mar Bartolomé", AssetsSprites.font,10, HEIGHT-550);

	
	public AboutScreen(Game game) {
		this.game = game;

		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.position.set(WIDTH / 2, HEIGHT / 2, 0);

		batch = new SpriteBatch();
		renderer = new ShapeRenderer();
		this.level = new LevelScreen();
		this.input = new AboutInput(this);

		 initObjects();
	}

	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	public void initObjects(){
		//Ui Objects
		uiCoconautsLogo = new UiObject(AssetsSprites.coconautsLogo, WIDTH/2-128,HEIGHT-300,256,256);
		uiBack = new UiObject(AssetsSprites.back, 10,10,64,64);

		uiLinkedinRephus = new UiObject(AssetsSprites.linkedin, 260, HEIGHT-520,32,32);
		uiEmailRephus = new UiObject(AssetsSprites.email, 300, HEIGHT-520,32,32);
		uiFacebookRephus = new UiObject(AssetsSprites.facebook, 340, HEIGHT-520,32,32);
		uiTwitterRephus = new UiObject(AssetsSprites.twitter, 380, HEIGHT-520,32,32);
		
		uiGPlusRephus = new UiObject(AssetsSprites.gplus, 420, HEIGHT-520,32,32);

		uiLinkedinMar = new UiObject(AssetsSprites.linkedin, 260, HEIGHT-570,32,32);
		uiEmailMar = new UiObject(AssetsSprites.email, 300, HEIGHT-570,32,32);
		uiFacebookMar = new UiObject(AssetsSprites.facebook, 340, HEIGHT-570,32,32);
		uiTwitterMar = new UiObject(AssetsSprites.twitter, 380, HEIGHT-570,32,32);
		uiGPlusMar = new UiObject(AssetsSprites.gplus, 420, HEIGHT-570,32,32);
		
		//Text objects
		textCoconauts = new TextObject("Coconauts",  AssetsSprites.font, WIDTH/2, HEIGHT-330, TextObject.ALIGN_CENTER,2,Color.WHITE);
		textCoconautsUrl = new TextObject("http://coconauts.net", AssetsSprites.font, WIDTH/2, HEIGHT-380,  TextObject.ALIGN_CENTER,1,Color.BLUE);
		textGameDeveloped = new TextObject("A game developed by", AssetsSprites.font, WIDTH/2, HEIGHT-450,  TextObject.ALIGN_CENTER,1,Color.WHITE);
		textJavier = new TextObject("Javier Rengel", AssetsSprites.font,10, HEIGHT-500);
		textMar = new TextObject("Mar Bartolomé", AssetsSprites.font,10, HEIGHT-550);

	}
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////
	@Override
	public void render(float delta) {

		update(delta);
		
		GLCommon gl = Gdx.gl;
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		level.render(delta);
		
		draw(delta);
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
		
	public void draw(float deltaTime) {


		cam.update();
		batch.setProjectionMatrix(cam.combined);

		//batch.disableBlending();
		batch.begin();
		
			drawMenu();

		batch.end();

		// SHAPE RENDERING
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		renderer.setProjectionMatrix(cam.combined);

		//drawRectangles();
		uiBack.drawBorder(renderer);
		textCoconautsUrl.drawBorder(renderer);
		uiLinkedinRephus.drawBorder(renderer);
	}

	public void drawMenu() {
		
		textCoconauts.draw(batch);
		textCoconautsUrl.draw(batch);
		textGameDeveloped.draw(batch);
		textJavier.draw(batch);
		textMar.draw(batch);

		uiBack.draw(batch);
		uiCoconautsLogo.draw(batch);
		
		uiLinkedinRephus.draw(batch);
		uiEmailRephus.draw(batch);
		uiFacebookRephus.draw(batch);
		uiTwitterRephus.draw(batch);
		uiGPlusRephus.draw(batch);
		
		uiLinkedinMar.draw(batch);
		uiEmailMar.draw(batch);
		uiFacebookMar.draw(batch);
		uiTwitterMar.draw(batch);
		uiGPlusMar.draw(batch);
	}
	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void update(float deltaTime) {

	}

	public void unlockMeetAchievement(){
		Main.actionResolver.unlockAchievement(Main.actionResolver.getAchievementId(Achievements.MEET_THE_CREATOR));
	}
	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.debug(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.debug(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.debug(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.debug(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.debug(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.debug(this.getClass().getName(), "hide");	}
}
