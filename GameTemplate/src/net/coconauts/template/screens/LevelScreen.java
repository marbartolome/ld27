package net.coconauts.template.screens;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import net.coconauts.template.common.Input;
import net.coconauts.template.common.LEvent;
import net.coconauts.template.objects.Enemy;
import net.coconauts.template.objects.GameObject;
import net.coconauts.template.objects.LevelGrid;
import net.coconauts.template.objects.LevelGrid.GridCell;
import net.coconauts.template.objects.Player;
import net.coconauts.template.objects.TextObject;
import net.coconauts.template.objects.UiObject;
import net.coconauts.template.system.AssetsSprites;
import net.coconauts.template.system.Options;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;

public class LevelScreen implements Screen {

	public static OrthographicCamera cam;
	public  SpriteBatch batch;
	public  final Matrix4 matrix = new Matrix4();	
//	public  Vector3 touchPoint;
	public ShapeRenderer renderer ;
	
	public Input input; 
	public static int WIDTH;
	public static int HEIGHT;

	public Player player;

	public List<Enemy> enemies ;
	public List<GameObject> blocks;

	private int rotation = 0;
	private float ejex = 0;

	public LEvent lEvent = new LEvent();
	
//	Random randGen = new Random();
	
//	public LevelGrid grid = new LevelGrid(12,12, 50, 50);
	public LevelGrid grid;
	
	public int countdown;
	public TextObject countdownText;
	
//	public Integer touchedX = null;
//	public Integer touchedY = null;
	
	public LevelScreen(){
		initSystem();
		initLevel();
	}
	
	public LevelScreen(LevelGrid grid, Player p) {
		this();
		this.grid = grid;
		this.player = p;
		grid.containerLevel = this;
	}

	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void initSystem() {

		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);  
        cam.position.set(WIDTH / 2, HEIGHT / 2, 0);

		batch = new SpriteBatch();
		batch.setColor(1,1,1,1);
		renderer = new ShapeRenderer();
		matrix.setToRotation(new Vector3(ejex, 0, 0), rotation);
//		touchPoint = new Vector3();
		
		input = new Input();

		countdownText = new TextObject(countdown+"",AssetsSprites.font, (int)(WIDTH/2)-20, HEIGHT-20, TextObject.ALIGN_LEFT,3f,Color.WHITE);
	}
	
	public void initLevel(){
		
		
		countdown = 10;
		
		enemies = new ArrayList<Enemy>();
//		blocks = new ArrayList<GameObject>();
		
//		initBlocks();
			
		
//		grid.populate();
//		initPlayer();	
		
		//initEnemies();
	}
	
//	private void initPlayer(){
//		player = new Player(AssetsSprites.player, 0f,0f);
//		
//		grid.placePlayer(player, grid.ramdonEmptyCell());
//		//player.setRectangle(AssetsSprites.player);
//	}
	
//	private void initBlocks(){
//		// Let's start with one block
//		
//		GameObject block = new UiObject(AssetsSprites.wall, 0f, 0f);
//		moveToRandomLevelPos(block);
//		blocks.add(block);
//		
//	}
	
//	public void moveToRandomLevelPos(GameObject o){
//		int x = randGen.nextInt(WIDTH);
//		int y = randGen.nextInt(HEIGHT);
//		o.x = x;
//		o.y = y;
//	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////
		
	@Override
	public void render (float delta) {
		update(delta);
		draw(delta);
	}
	
	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
		
	public void draw (float deltaTime) {

		Gdx.gl.glClearColor(05f, 0.5f, 0.1f, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
		//Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		cam.update();
		
		//SPRITES RENDERING
		batch.setProjectionMatrix(cam.combined);
		
		batch.setTransformMatrix(matrix);
		batch.begin();
	
		lEvent.drawEvents(batch);
		
		grid.draw(batch);
		
		if(!grid.isMaster) countdownText.draw(batch);
//		player.draw(batch);
//		for (Enemy e : enemies){
//			e.draw(batch);
//		}
//		for (GameObject block : blocks){
//			block.draw(batch);
//		}
		

					
		batch.end();
		
		//SHAPE RENDERING
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glLineWidth(5);

		renderer.setProjectionMatrix(cam.combined);
		
//		drawPath();

	}
	
//	public void drawPath(){
//		if (touchedX == null || touchedY==null) return;
//		
//		GridCell selectedCell = grid.getCellFromScreenCoords(this.touchedX, this.touchedY);
//		GridCell startCell;
//		int startX;
//		int startY;
//		int endX;
//		int endY;
//		
//		if (grid.isEmpty(selectedCell)
//				&& grid.isInlineWithPlayer(selectedCell)){
//			// draw a line from player cell to selected cell
//			if (grid.playerCell.gridXindex == selectedCell.gridXindex){
//				// horizonatl line.
//				// start a cell left of right from the player
//				int dir = grid.xDirFromPlayer(selectedCell);
//				startCell = grid.getCellFromGridCoords(grid.playerCell.gridXindex+dir, grid.playerCell.gridYindex);
//				startX = (int) (startCell.screenXPos + grid.cellXSize/2);
//				startY = (int) (startCell.screenYPos + grid.cellYSize/2);
//				endX = (int) (selectedCell.screenXPos + grid.cellXSize/2);
//				endY = (int) (selectedCell.screenYPos + grid.cellYSize/2);
//			} else {
//				//vertical line
//				// horizonatl line.
//				// start a cell left of right from the player
//				int dir = grid.xDirFromPlayer(selectedCell);
//				startCell = grid.getCellFromGridCoords(grid.playerCell.gridXindex, grid.playerCell.gridYindex+dir);
//				startX = (int) (startCell.screenXPos + grid.cellXSize/2);
//				startY = (int) (startCell.screenYPos + grid.cellYSize/2);
//				endX = (int) (selectedCell.screenXPos + grid.cellXSize/2);
//				endY = (int) (selectedCell.screenYPos + grid.cellYSize/2);
//			}
//			
//			renderer.begin(ShapeType.Line);
//			renderer.setColor(Color.WHITE);
//			renderer.line(startX,startY,endX,endY);
//			renderer.end();
//			
//		} else {
//			// draw a nasty X on top of the cell
////			
////			UiObject x = new UiObject(AssetsSprites.invalid, selectedCell.screenXPos, selectedCell.screenYPos);
////			x.draw(b)
//		}
//		
//		 
//	}
	

	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////
		
	public void update (float deltaTime) {

		lEvent.updateEvents();

		collision();
		
		player.update();
		
		for (Enemy e : enemies){
			e.update();
		}
		
		countdownText.str = ""+countdown;
		
		
	}
	public void collision(){

		Iterator<Enemy> it = enemies.iterator();
		Enemy e;
		while  (it.hasNext()) {
			e = (Enemy) it.next();

			if (player.rendered && e.collide(player) ) {
		
				it.remove();
			} 
		}
	}

	public boolean isLevelEnded(){
		return enemies.isEmpty();
	}
	
	public void tick(){
		
		if (countdown>0 && !grid.isMaster) countdown--;
	}

	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.debug(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.debug(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.debug(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.debug(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.debug(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.debug(this.getClass().getName(), "hide");	}

}
