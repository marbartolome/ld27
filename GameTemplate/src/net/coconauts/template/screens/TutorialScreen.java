/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package net.coconauts.template.screens;

import net.coconauts.template.controllers.TutorialInput;
import net.coconauts.template.interfaces.ScoreResolver.Achievements;
import net.coconauts.template.objects.GameObject;
import net.coconauts.template.objects.UiObject;
import net.coconauts.template.system.AssetsSprites;
import net.coconauts.template.system.Main;
import net.coconauts.template.system.Options;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GLCommon;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;

public class TutorialScreen implements Screen {


	public Game game;
	public LevelScreen level;
	public static OrthographicCamera cam;
	public SpriteBatch batch;
	public Vector3 touchPoint;
	public ShapeRenderer renderer;
	public int currentSlide = 0;
	public final String[] slidePaths = new String[]{};

	public TutorialInput input;
	public static int WIDTH;
	public static int HEIGHT;
	 
	 public GameObject uiBack;// = new UiObject(AssetsSprites.back, 10,10,64,64);
	 public GameObject uiPrev;// = new UiObject(AssetsSprites.prev, WIDTH/2-70, 10, 64,64);
	public GameObject uiNext ;//= new UiObject(AssetsSprites.next, WIDTH/2+20, 10, 64,64);
	
	public TutorialScreen(Game game) {
		this.game = game;

		WIDTH = Options.width.getInt();
		HEIGHT = Options.height.getInt();
		cam = new OrthographicCamera(WIDTH, HEIGHT);
		cam.position.set(WIDTH / 2, HEIGHT / 2, 0);

		batch = new SpriteBatch();
		renderer = new ShapeRenderer();
		//this.level = new LevelScreen();
		this.input = new TutorialInput(this);


		initObjects();
	}

	// ///////////////////////////////////////////////////////////////////////////
	// INIT METHODS
	// ///////////////////////////////////////////////////////////////////////////
	public void initObjects(){
	    uiBack = new UiObject(AssetsSprites.back, 10,10,64,64);
		uiPrev = new UiObject(AssetsSprites.prev, WIDTH/2-70, 10, 64,64);
		uiNext = new UiObject(AssetsSprites.next, WIDTH/2+20, 10, 64,64);
		
	}
	// ///////////////////////////////////////////////////////////////////////////
	// RENDER
	// ///////////////////////////////////////////////////////////////////////////
	
	@Override
	public void render(float delta) {

		update(delta);
		
		GLCommon gl = Gdx.gl;
		gl.glClearColor(0, 0, 0, 1);
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		//level.render(delta);
		
		draw(delta);
	}

	// ///////////////////////////////////////////////////////////////////////////
	// DRAW METHODS
	// ///////////////////////////////////////////////////////////////////////////
	
	public void draw(float deltaTime) {

		cam.update();
		batch.setProjectionMatrix(cam.combined);

		//batch.disableBlending();
		batch.begin();
		
			drawSlide(currentSlide);
			drawButtons();

		batch.end();

		// SHAPE RENDERING
		Gdx.gl.glEnable(GL10.GL_BLEND);
		Gdx.gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		renderer.setProjectionMatrix(cam.combined);

	}
	
	public void drawButtons() {
		// Draw options
		uiBack.draw(batch);
		if (currentSlide > 0)
			uiPrev.draw(batch);
		if (currentSlide < AssetsSprites.MAX_SLIDES - 1)
			uiNext.draw(batch);
	}

	public void drawSlide(int slideNum) {
		float slideHeight = HEIGHT - 65;
		batch.draw(AssetsSprites.tutorialSlides[slideNum], 0, 65, WIDTH,slideHeight);
	}
	// ///////////////////////////////////////////////////////////////////////////
	// UPDATE METHODS
	// ///////////////////////////////////////////////////////////////////////////

	public void update(float deltaTime) {

	}
	
	
	public void nextSlide(){
		if (currentSlide < AssetsSprites.MAX_SLIDES-1) currentSlide += 1;
		if (currentSlide == AssetsSprites.MAX_SLIDES-1)
			Main.actionResolver.unlockAchievement(Main.actionResolver.getAchievementId(Achievements.READY_TO_ROCK));

	}
	
	public void prevSlide(){
		if (currentSlide > 0) currentSlide -= 1;
		
	}
	// ///////////////////////////////////////////////////////////////////////////
	// EVENT METHODS
	// ///////////////////////////////////////////////////////////////////////////

	@Override
	public void pause() {Gdx.app.debug(this.getClass().getName(), "pause");}

	@Override
	public void resume() {	Gdx.app.debug(this.getClass().getName(), "resume");	}

	@Override
	public void dispose() {	Gdx.app.debug(this.getClass().getName(), "dispose");	}

	@Override
	public void resize(int width, int height) {	Gdx.app.debug(this.getClass().getName(), "resize");	}

	@Override
	public void show() {Gdx.app.debug(this.getClass().getName(), "show");	}

	@Override
	public void hide() {Gdx.app.debug(this.getClass().getName(), "hide");	}
}
