package net.coconauts.template.controllers;

import net.coconauts.template.common.Input;
import net.coconauts.template.screens.LevelScreen;
import net.coconauts.template.screens.MainScreen;
import net.coconauts.template.screens.TutorialScreen;

import com.badlogic.gdx.graphics.OrthographicCamera;


public class TutorialInput extends Input {
	
	TutorialScreen screen ;
	
	public TutorialInput(TutorialScreen screen){
		this.screen = screen;
	}
	
	@Override
	public boolean scrolled(int amount) {
		super.scrolled(amount);
		
		OrthographicCamera cam = LevelScreen.cam;

		cam.zoom += (float) (amount)/10;//position.set(cam.position.x , cam.position.y+amount, cam.position.z);
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {

		super.touchDown(x, y, pointer, button);
		touchDownTime = 0;

		if (screen.uiBack.click(touchDownX, touchDownY)){
			screen.game.setScreen(new MainScreen(screen.game));
		} else if (screen.uiNext.click(touchDownX, touchDownY)){
			screen.nextSlide();
		} else if (screen.uiPrev.click(touchDownX, touchDownY)){
			screen.prevSlide();
		}
		return false;
	}
}
