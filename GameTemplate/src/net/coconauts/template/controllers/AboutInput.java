package net.coconauts.template.controllers;

import net.coconauts.template.common.Input;
import net.coconauts.template.screens.AboutScreen;
import net.coconauts.template.screens.MainScreen;
import net.coconauts.template.system.Main;

public	class AboutInput extends Input {
	
	AboutScreen screen ;
	
	public AboutInput(AboutScreen screen){
		this.screen = screen;
	}
	
	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {

		super.touchDown(x, y, pointer, button);
		touchDownTime = 0;

		if (screen.uiBack.click(touchDownX, touchDownY)) {
			screen.game.setScreen(new MainScreen(screen.game));
		} else if (screen.uiCoconautsLogo.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("http://coconauts.net");
			unlockMeetAchievement();
		} else if (screen.uiLinkedinRephus.click(touchDownX, touchDownY)) {
			Main.nativeFunctions
					.openURL("http://es.linkedin.com/pub/javier-rengel-jimenez/56/865/432/en");
			unlockMeetAchievement();
		} else if (screen.uiEmailRephus.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.sendMail("rephus@coconauts.net");
			unlockMeetAchievement();
		} else if (screen.uiFacebookRephus.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("https://www.facebook.com/rephus");
			unlockMeetAchievement();
		} else if (screen.uiTwitterRephus.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("http://twitter.com/rephus");
			unlockMeetAchievement();
		} else if (screen.uiGPlusRephus.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("https://plus.google.com/u/0/100118526666395537169/");
			unlockMeetAchievement();
		} else if (screen.uiLinkedinMar.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("http://es.linkedin.com/pub/marimar-bartolome-rueda/18/a9/544");
			unlockMeetAchievement();
		} else if (screen.uiEmailMar.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.sendMail("marbu@coconauts.net");
			unlockMeetAchievement();
		} else if (screen.uiFacebookMar.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("https://www.facebook.com/marbuface");
			unlockMeetAchievement();
		} else if (screen.uiTwitterMar.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("http://twitter.com/marbu");
			unlockMeetAchievement();
		} else if (screen.uiGPlusMar.click(touchDownX, touchDownY)) {
			Main.nativeFunctions.openURL("https://plus.google.com/108072729064674902442/");
			unlockMeetAchievement();
		}

		return false;
	}
	
	public void unlockMeetAchievement(){
		
	}
}
