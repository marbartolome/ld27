package net.coconauts.template.controllers;

import com.badlogic.gdx.Gdx;

import net.coconauts.template.common.Input;
import net.coconauts.template.objects.LevelGrid.GridCell;
import net.coconauts.template.screens.GameScreen;
import net.coconauts.template.screens.MainScreen;
import net.coconauts.template.system.AssetsSounds;

public class GameInput extends Input {
	
	GameScreen screen ;
	
	public GameInput(GameScreen screen){
		this.screen = screen;
	}
	

	
	@Override
	public boolean keyDown(int keycode) {
		if(!screen.gameOver){
			int movementX = 0;
			int movementY = 0;
			if (keycode == com.badlogic.gdx.Input.Keys.UP) movementY=1;
			else if (keycode == com.badlogic.gdx.Input.Keys.DOWN) movementY=-1;
			else if (keycode == com.badlogic.gdx.Input.Keys.LEFT) movementX=-1;
			else if (keycode == com.badlogic.gdx.Input.Keys.RIGHT) movementX=1;
			
			int playerXindex = (int) screen.level.grid.playerCell.gridXindex;
			int playerYindex = (int) screen.level.grid.playerCell.gridYindex;
			
			Gdx.app.log("move", "xCmp:"+movementX+", yCmp:"+movementY);
			
	//		GridCell newPlyCell = screen.level.grid.getCellFromGridCoords(playerXindex+movementX, playerYindex+movementY);
			screen.level.grid.placePlayer(screen.level.player, playerXindex+movementX, playerYindex+movementY);
		}
		
		return false;
	}



	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return super.keyUp(keycode);
	}



	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {

		super.touchDown(x, y, pointer, button);
		
		x = (int) touchDownX;
		y = (int)touchDownY;
		if(!screen.gameOver){
			int playerXindex = (int) screen.level.grid.playerCell.gridXindex;
			int playerYindex = (int) screen.level.grid.playerCell.gridYindex;
			
			GridCell  touchedCell = screen.level.grid.getCellFromScreenCoords(x, y);
			
			int xCmp = Double.compare(touchedCell.gridXindex, playerXindex);
			int yCmp = Double.compare(touchedCell.gridYindex, playerYindex);
			
			Gdx.app.log("move", "xCmp:"+xCmp+", yCmp:"+yCmp);
			
			GridCell newPlyCell = screen.level.grid.getCellFromGridCoords(playerXindex+xCmp, playerYindex+yCmp);
			screen.level.grid.placePlayer(screen.level.player, newPlyCell);
			
//			screen.level.player.move(touchDownX, touchDownY);
			
			
		}
		
		if (screen.rBack.contains(x,y)){
			screen.game.setScreen(new MainScreen(screen.game));
			AssetsSounds.bgMusic.stop();
		}
		
		
		return false;
	}
    @Override 
	public boolean touchUp(int x, int y, int pointer, int button) {

		super.touchUp(x, y, pointer, button);
		
		

		if (this.touchDownTime != 0) {
		
		}
		return false;
	}
}
