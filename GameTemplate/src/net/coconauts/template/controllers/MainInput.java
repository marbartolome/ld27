package net.coconauts.template.controllers;

import net.coconauts.template.common.Input;
import net.coconauts.template.screens.AboutScreen;
import net.coconauts.template.screens.GameScreen;
import net.coconauts.template.screens.LevelScreen;
import net.coconauts.template.screens.MainScreen;
import net.coconauts.template.screens.TutorialScreen;
import net.coconauts.template.system.Main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;

public class MainInput extends Input {
	
	MainScreen screen ;
	
	public MainInput(MainScreen screen){
		this.screen = screen;
	}
	
	@Override
	public boolean scrolled(int amount) {
		super.scrolled(amount);
		
		OrthographicCamera cam = LevelScreen.cam;

		cam.zoom += (float) (amount)/10;//position.set(cam.position.x , cam.position.y+amount, cam.position.z);
		return false;
	}

	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		Gdx.app.log("MainInput","touchDown - x: "+x+", y: "+y);
		super.touchDown(x, y, pointer, button);
		touchDownTime = 0;

//		if (screen.uiGPSLogin.click(touchDownX, touchDownY)) {
//			Main.actionResolver.singedIn();
//		} else if (screen.uiGPSLogout.click(touchDownX, touchDownY)) {
//			Main.actionResolver.singOut();
//		} else if (screen.uiAchievements.click(touchDownX, touchDownY)) {
//			Main.actionResolver.achievementScreen();
//		} else if (screen.uiLeaderboards.click(touchDownX, touchDownY)) {
//			Main.actionResolver.scoreBoardScreen();
		//} else if (screen.textGameTitle.click(touchDownX, touchDownY)) {
		 if (screen.textPlay.click(touchDownX, touchDownY)) {
			screen.game.setScreen(new GameScreen(screen.game,1));
//		} else if (screen.textTutorial.click(touchDownX, touchDownY)) {
//			screen.game.setScreen(new TutorialScreen(screen.game));
//		} else if (screen.textAbout.click(touchDownX, touchDownY)) {
//			Gdx.app.log("MainInput","About click ");
//			screen.game.setScreen(new AboutScreen(screen.game));
		}
		
		return false;
	}
}