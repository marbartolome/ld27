package net.coconauts.template.common;

import java.util.Arrays;

import net.coconauts.template.objects.Particle;
import net.coconauts.template.system.AssetsSprites;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Explosion implements Event {

	public Particle[] particles; // particles in the explosion
	public int size; // number of particles
	public int state; // whether it's still active or not

	public Explosion(int particleNr, int x, int y) {
		this.state = STATE_ALIVE;
		this.particles = new Particle[particleNr];
		for (int i = 0; i < this.particles.length; i++) {
			Particle p = new Particle(x, y);
			this.particles[i] = p;
		}
		this.size = particleNr;
	}

	public boolean isAlive() {
		return this.state == STATE_ALIVE;
	}

	public boolean isDead() {
		return this.state == STATE_DEAD;
	}

	public void update() {
		if (this.state != STATE_DEAD) {
			boolean isDead = true;
			for (int i = 0; i < this.particles.length; i++) {
				if (this.particles[i].isAlive()) {
					this.particles[i].update();
					isDead = false;
				}
			}
			if (isDead)
				this.state = STATE_DEAD;
		}
	}

	@Override
	public String toString() {
		return "Explosion state=" + state + " [particles="
				+ Arrays.toString(particles) + ", size=" + size + "]";
	}

	@Override
	public void draw(SpriteBatch batch) {
		for (int i = 0; i < this.particles.length; i++) {
			Particle p = this.particles[i];
			if (p.isAlive()) {
				batch.draw(AssetsSprites.particle, (int) p.x, (int) p.y);
			}
		}
	}

}
