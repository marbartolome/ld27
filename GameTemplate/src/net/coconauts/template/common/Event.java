package net.coconauts.template.common;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface Event {
	
	public static final int STATE_ALIVE 	= 0;	// at least 1 particle is alive
	public static final int STATE_DEAD 	= 1;	// all particles are dead
		
	public boolean isAlive() ;
	public boolean isDead();

	public void update() ;
	public void draw(SpriteBatch batch);
}
