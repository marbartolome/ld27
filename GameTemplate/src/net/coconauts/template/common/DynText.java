package net.coconauts.template.common;

import net.coconauts.template.system.AssetsSprites;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class DynText implements Event {

	public String str;
	public int x, y;
	public float size;
	private int ms_alive;
	private long initTime ;
	private int state; // whether it's still active or not
	private Color color;
	private float alpha;

	public DynText(String str, int x, int y, float size, Color color,
			int seconds) {
		this.state = STATE_ALIVE;

		this.str = str;
		this.x = x;
		this.y = y;
		this.color = color;
		this.size = size;
		this.ms_alive = seconds;

		alpha = 1;

		initTime = System.currentTimeMillis();
	}

	// helper methods -------------------------
	public boolean isAlive() {
		return this.state == STATE_ALIVE;
	}

	public boolean isDead() {
		return this.state == STATE_DEAD;
	}

	public void update() {
		long actual_time = System.currentTimeMillis();

		float living_time = (float) (actual_time - initTime);
		float remaining_time = living_time / ms_alive;
		alpha = 1 - remaining_time;
		if (actual_time - initTime >= ms_alive) { // reached the end if its
													// life
			this.state = STATE_DEAD;
		}
	}

	public Color getColor() {

		color.a = alpha;
		return color;
	}

	@Override
	public String toString() {
		return "DynText [str=" + str + ", x=" + x + ", y=" + y + ", size="
				+ size + ", color=" + color + ", alpha=" + alpha + "]";
	}

	@Override
	public void draw(SpriteBatch batch) {

		AssetsSprites.font.setColor(this.getColor());
		AssetsSprites.font.setScale(this.size);
		// CENTER TEXT
		TextBounds bounds = AssetsSprites.font.getBounds(this.str);
		float x = this.x - bounds.width / 2;
		float y = this.y + bounds.height / 2;

		AssetsSprites.font.draw(batch, this.str, x, y);
	}
}
