package net.coconauts.template.interfaces;

public interface Native {
 public void openURL(String url);
 public void sendMail(String address);
}
