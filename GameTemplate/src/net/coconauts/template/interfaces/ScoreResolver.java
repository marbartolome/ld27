package net.coconauts.template.interfaces;

public interface ScoreResolver {
	

	public void log();
	public boolean isEnabled();
	public void submitScore( String mode,  long score) ;
	public void unlockAchievement(String achievement) ;
	public void incrAchievement(String achievement, int i) ;
	public boolean isSigned();
	public void singedIn();
	public void singOut();
	public void scoreBoardScreen();
	public void achievementScreen();
	//List of achievements
	public String getAchievementId(Achievements id);
	//List of scoreboards
	public String getScoreboardArcade();
	public String getScoreboardTimeAttack();
	public String getScoreboardUnsurvival();
	
	
	public enum Achievements {
		FINISH_ARCADE,
		ROCK_MASTER,
		NO_SIN_THROW_FIRST_STONE,
		FINISH_UNSURVIVAL,
		FINISH_TIME_ATTACK,
		LOOK_MA_NO_ASTEROIDS,
		I_WILL_SURVIVE,
		SURVIVAL_HORROR,
		ASTEROIDS_ADDICT,
		ROCK_N_ROLL,
		LAST_ROCK_STANDING,
		ON_PURPOSE,
		GO_THROUGH_TUTORIAL_FIRST,
		READY_TO_ROCK,
		MINUTE_ATTACK,
		HALF_MINUTE_VILLAIN,
		BEAM_ME_UP,
		MORE_LIFES_THAN_A_CAT,
		MEET_THE_CREATOR;
	}
}