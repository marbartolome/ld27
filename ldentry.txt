In the depths of a dimensional labbyrinth hides the Master of All Magic, a wizard so powerful and wise that it's said to know all the misteries of the universe. 

Only those who survive the labbyrinth will be considered worthy of the Master's attention, and will get invited to a cup of tea. And maybe then, be instructed in the secrets of magic.

The labbyrinth is a randomly generated maze of walls and warp doors. One of the doors will lead to the Master's lair, but the others might take you anywhere. Each room in the labbyrinth has a mind of its own, and will only allow you to stay on it for 10 seconds. After that time it will reclaim your soul, and the Master will have it for breakfast in the morning.

So you'll have to be fast and try to remember your past steps if you want to keep you life and meet the Master.

Good luck.

----

This is my first LD entry, as and a matter of fact my first game as well! So I apologize beforehand for the crap =P

For movement you can use the keyboard arrows or the mouse. At the beggining you'll need to click with on "play" with the mouse. While playing, you can go back by clicking on the back arrow on the bottom left corner. This is useful if you find yourself stuck, so you can regenerate a new dungeon from scratch.

For the coding I used Java and LibGdx. I based my game over a LibGdx template made by http://www.ludumdare.com/compo/author/rephus/.

For the graphics I used openclipart.org.

For the music the awesome autotracker.

All in the game is pretty crap. The code is the dirtiest thing even, and the dungeon generator doesn't even have any constraints about the dungeons being solvable (so the warp door may be stuck behind a wall, or all of the warp doors in the first level would point to themselves, things like that).

But I managed to finish a game having no idea about what I was doing, so for me it's good enough :o)


