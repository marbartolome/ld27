package net.coconauts.template;

import net.coconauts.template.interfaces.Native;
import net.coconauts.template.interfaces.ScoreResolver;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	
	public static ScoreResolver resolver;
	public static Native nativeFunctions;
	
	public static void main(String[] args)  {
		resolver = new ActionResolver();
		nativeFunctions = new NativeFunctions();
		
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "GameTemplate";
		cfg.useGL20 = false;
		//cfg.width = 768;
		//cfg.height = 1280;
		cfg.width = 600;
		cfg.height = 600;

		new LwjglApplication(new net.coconauts.template.system.Main(resolver,nativeFunctions), cfg);
	}
}
