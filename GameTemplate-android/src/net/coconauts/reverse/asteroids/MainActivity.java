package net.coconauts.reverse.asteroids;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import net.coconauts.template.interfaces.Native;
import net.coconauts.template.interfaces.ScoreResolver;
import net.coconauts.template.system.Main;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.games.GamesClient;

public class MainActivity extends AndroidApplication implements ScoreResolver, GameHelper.GameHelperListener, Native{

	public static GameHelper  mGameHelper ;
	GamesClient mGamesClient = null;
	final int RC_RESOLVE = 5000, RC_UNUSED = 5001;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mGameHelper = new GameHelper(this);
	    mGameHelper.setup(this, 1);
	        
		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		cfg.useGL20 = false;

		initialize(new Main(this,this), cfg);
	}

	@Override
	protected void onStart() {
	    super.onStart();
	    Log.d("gps","onStart");
	    mGameHelper.onStart(this);
	}

	@Override
    public void onStop() {
        super.onStop();
        mGameHelper.onStop();
    }
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
//ScoreResolver

	@Override
	public void log() {
		
	}

	@Override
	public void submitScore(String mode, long score) {
		mGameHelper.getGamesClient().submitScore(mode, score);
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public void onSignInFailed() {
		Log.d("gps","onSignInFailed");
	}

	public void restart(){
		
		finish();
		startActivity(getIntent());
	}
	@Override
	public void onSignInSucceeded() {
		Log.d("gps","onSignInSucceeded");
	}
	
	@Override
	public void incrAchievement(String achievement, int i) {
		if (isSigned() && mGameHelper.getGamesClient().isConnected())
		mGameHelper.getGamesClient().incrementAchievement(achievement, i);
	}
	@Override
	public void unlockAchievement(String achievement) {
		if (isSigned() && mGameHelper.getGamesClient().isConnected())
		mGameHelper.getGamesClient().unlockAchievement(achievement);
	}

	@Override
	public boolean isSigned() {
		
		return mGameHelper.isSignedIn();
	}

	@Override
	public void singedIn() {
		Log.d("gps","singedIn");
		mGameHelper.beginUserInitiatedSignIn();		
		
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.d("gps","requestCode "+requestCode+" resultCode "+resultCode+" data "+data);

		if ((requestCode == 9001) && (resultCode == RESULT_OK) ) {//Google play services startResolutionForResult
			mGameHelper = new GameHelper(this);
		    mGameHelper.setup(this, 1);
			onStart();
			singedIn();
		}
	}

	@Override
	public void singOut() {
		Log.d("gps","singOut");
		mGameHelper.signOut();		
	}

	@Override
	public void scoreBoardScreen() {
		startActivityForResult(mGameHelper.getGamesClient().getAllLeaderboardsIntent(), RC_UNUSED);
	}
	@Override
	public void achievementScreen() {
		startActivityForResult(mGameHelper.getGamesClient().getAchievementsIntent(), RC_UNUSED);
		
	}

	@Override public String getAchievementId(Achievements achType){
		int achId = getResources().getIdentifier(achType.name().toLowerCase(), "string", getPackageName());
		return getString(achId);
	}
	
	@Override public String getScoreboardArcade() {return getString(R.string.leaderboard_arcade);}
	@Override public String getScoreboardTimeAttack() {return getString(R.string.leaderboard_time_attack);}
	@Override public String getScoreboardUnsurvival() {return getString(R.string.leaderboard_unsurvival);}

	@Override
	public void openURL(String url) {
		 Intent viewIntent = new Intent("android.intent.action.VIEW", 
		            Uri.parse(url));
		        startActivity(viewIntent);  
	}

	@Override
	public void sendMail(String address) {
		openURL("mailto:"+address);
	}

}