#!/bin/bash

#USAGE:
# -c <arg>   query for color palettes related to <arg>
# -d <arg>   output images will be <arg> pixels wide/high
# -n <arg>   <arg> images extracted per image source (Google, Wikimedia,etc.)
# -o <arg>   images saved to filepath <arg>
# -rc        choose color palette randomly from set
# -rs        choose images randomly from an initial set
# -sgi       search google images
# -soc       search openclipart
# -swm       search wikimedia commons

java -jar spritely-cli.jar -n 10 -o sprites -soc $1